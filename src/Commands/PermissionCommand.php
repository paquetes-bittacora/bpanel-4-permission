<?php

namespace Bittacora\Permission\Commands;

use Illuminate\Console\Command;

class PermissionCommand extends Command
{
    public $signature = 'permission';

    public $description = 'My command';

    public function handle()
    {
        $this->comment('All done');
    }
}
