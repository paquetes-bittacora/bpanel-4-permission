<?php

namespace Bittacora\Permission;

use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Bittacora\Permission\Commands\PermissionCommand;


class PermissionServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('permission')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_permission_table');
    }

    public function register(){

        $this->app->bind('permission', function($app){
            return new Permission();
        });
    }

    public function boot(){
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadViewsFrom(__DIR__. '/../resources/views', 'permission');
        $this->loadTranslationsFrom(__DIR__ .'/../resources/lang', 'permission');
        $this->loadTranslationsFrom(__DIR__ .'/../resources/lang', 'role');

    }
}
