<?php

namespace Bittacora\Permission;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\Permission\Permission
 */
class PermissionFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'permission';
    }
}
