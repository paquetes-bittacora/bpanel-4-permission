<?php

namespace Bittacora\Permission\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4Users\Models\User;
use Bittacora\AdminMenu\Models\ModuleModel;
use Bittacora\Permission\Http\Requests\StoreRoleRequest;
use Bittacora\Permission\Http\Requests\UpdateRoleRequest;
use Illuminate\Support\Facades\Session;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('role.index');
        $roles = Role::all();
        return view('permission::role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('role.create');
        return view('permission::role.create',
            [
                'modules' => ModuleModel::all(),
                'users' => User::getAllAsArrayKeyValue()
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRoleRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRoleRequest $request)
    {
        $this->authorize('role.create');
        /**
         * @var Role $role
         */
        $role = Role::create(['name' => $request->input('name'), 'guard_name' => 'web']);
        $role->syncPermissions($request->input('permission'));
        $role->users()->sync($request->input('users'));

        Session::put('alert-success', 'Rol creado correctamente');
        return redirect(route('role.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  Role $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $this->authorize('role.show');
        return view('permission::role.show', [
            'modules' => ModuleModel::all(),
            'role' => $role,
            'roleUsers' => $role->users()->get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Role $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $this->authorize('role.edit');
        $modules = ModuleModel::all();
        $rolePermissions = $role->getAllPermissions()->pluck('id')->toArray();
        $roleUsers = $role->users()->pluck('id')->toArray();
        $users =  User::getAllAsArrayKeyValue();
        return view('permission::role.edit', compact('role', 'modules', 'rolePermissions', 'roleUsers', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRoleRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoleRequest $request, Role $role)
    {
        $this->authorize('role.edit');
        Role::where('id', $role->id)->update(['name' => $request->input('name')]);
        $role->syncPermissions($request->input('permission'));
        $role->users()->sync($request->input('users'));
        Session::put('alert-success', 'Rol editado correctamente');
        return redirect(route('role.edit', ['role' => $role]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $this->authorize('role.destroy');
        $role->delete();
        Session::put('alert-success', 'Rol eliminado correctamente');
        return redirect(route('role.index'));
    }
}
