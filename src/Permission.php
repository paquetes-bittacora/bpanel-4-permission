<?php

namespace Bittacora\Permission;

use Illuminate\Support\Facades\Config;

class Permission
{
    public function getModulePermissions(string $modulePrefix)
    {
        return \Spatie\Permission\Models\Permission::all()->filter(function ($value, $key) use($modulePrefix) {
            return strpos($value->name,$modulePrefix) === 0;
        });

    }
}
