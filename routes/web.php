<?php

use Bittacora\Permission\Http\Controllers\PermissionController;
use Bittacora\Permission\Http\Controllers\RoleController;
use Illuminate\Support\Facades\Route;


Route::group(['middleware' => ['web', 'auth', 'admin-menu']], function() {
    Route::get('/bpanel/role', [RoleController::class, 'index'])->name('role.index');
    Route::get('/bpanel/role/{role}/show', [RoleController::class, 'show'])->name('role.show');
    Route::get('/bpanel/role/{role}/edit', [RoleController::class, 'edit'])->name('role.edit');
    Route::get('/bpanel/role/create', [RoleController::class, 'create'])->name('role.create');
    Route::post('/bpanel/role/store', [RoleController::class, 'store'])->name('role.store');
    Route::put('/bpanel/role/{role}', [RoleController::class, 'update'])->name('role.update');
    Route::delete('/bpanel/role/{role}', [RoleController::class, 'destroy'])->name('role.destroy');
});

