<?php

namespace Bittacora\Permission\Database\Seeders;

use Bittacora\Permission\Database\Seeders\seeds\PermissionSeeder;
use Bittacora\Permission\Database\Seeders\seeds\RoleSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
//            RoleSeeder::class,
//            PermissionSeeder::class
        ]);
    }
}
