<?php

namespace Bittacora\Permission\Database\Seeders\seeds;

use App\Models\User;
use Bittacora\AdminMenu\AdminMenuFacade;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::findOrCreate('admin');

        $module = AdminMenuFacade::createModule('configuration', 'role', 'Roles', 'fa fa-user');
        AdminMenuFacade::createAction($module->key, 'Listar', 'index', 'fa fa-bars');
        AdminMenuFacade::createAction($module->key, 'Añadir', 'create', 'fa fa-plus');

        $adminRole = Role::findByName('admin');

        $permissions = ['index', 'create', 'show', 'edit', 'destroy'];

        foreach($permissions as $permission){
            $permission = Permission::create(['name' => 'role.'.$permission]);
            $adminRole->givePermissionTo($permission);
        }

    }
}
