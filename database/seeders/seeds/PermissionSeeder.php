<?php

namespace Bittacora\Permission\Database\Seeders\seeds;

use Bittacora\AdminMenu\AdminMenuFacade;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $module = AdminMenuFacade::createModule('configuration', 'permission', 'Permisos', 'fa fa-key');
//        AdminMenuFacade::createAction($module->key, 'Listar', 'index', 'fa fa-bars');
//        AdminMenuFacade::createAction($module->key, 'Añadir', 'create', 'fa fa-plus');

        $permissions = ['index', 'create', 'show', 'edit', 'destroy'];

        $adminRole = Role::findOrCreate('admin');


        foreach($permissions as $permission){
            $permission = Permission::create(['name' => 'permission.'.$permission]);
            $adminRole->givePermissionTo($permission);
        }
    }
}
