@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Roles')

@section('content')

        @if(!empty($roleUsers))
            <table class="table">
                <div class="card-header bgc-primary-d1 text-white border-0">
                    <h4 class="text-120 mb-0">
                        <span class="text-90">{{ __('permission::role.associated_users') }}</span>
                    </h4>
                </div>
                <tbody>
                @foreach($roleUsers as $user)
                    <tr>
                        <td>{{$user->name}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif

        @foreach($modules as $module)
            <div class="table-responsive table-bpanel">
                <table class="table table-hover table-2-columns">
                    <thead class="bgc-primary-d1 text-white ">
                        <tr>
                            <th scope="col" colspan="2">
                                <h4 class="text-120 mb-0">
                                    Permisos del módulo: <b>{{$module->title}}</b>
                                </h4>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach(\Bittacora\Permission\PermissionFacade::getModulePermissions($module->key) as $permission)
                            <tr>
                                @if($module->key == 'role')
                                    <td>{{ __("permission::$permission->name") }}</td>
                                @else
                                    <td>{{ __("$module->key::$permission->name") }}</td>
                                @endif
                                <td class="text-center">@if($role->hasPermissionTo($permission)) <i class="fa fa-check"></i> @endif</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endforeach


@endsection
