@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Roles')

@section('content')

    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('permission::role.role_list') }}</span>
            </h4>
        </div>
        <div class="fixed-table-body table-bpanel">
            <table class="table text-dark-m2 text-95 bgc-white ml-n1px table-hover table-2-columns " id="table">
                <thead class="bgc-white text-grey text-uppercase text-80">
                <tr>
                    <th data-field="name">
                        <div class="">Nombre</div>
                    </th>
                    <th data-field="name">
                        <div class="text-center">Acciones</div>
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach ($roles as $role)
                    <tr data-has-detail-view="true">
                        <td>{{$role->name}}</td>
                        <td class="text-center">
                            <div class="action-buttons">
                                <a class="btn mx-1 btn-outline-info" href="{{route('role.show',$role)}}">
                                    <i class="fa fa-search"></i>
                                </a>
                                    <a class="btn mx-1 btn-outline-success"  href="{{route('role.edit', $role)}}">
                                        <i class="fa fa-cogs"></i>
                                    </a>
                                <form method="POST" class="d-inline mx-1"
                                      action="{{route('role.destroy', $role)}}">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-outline-danger"
                                            onclick="return confirm('¿Está seguro de querer borrar el rol?')">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
