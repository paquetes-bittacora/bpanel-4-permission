@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Roles')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120">
                <span class="text-90">{{ __('permission::role.editing_role') }}: {{$role->name}}</span>
            </h4>
        </div>
        <form class="mt-lg-3" autocomplete="off" method="post" action="{{route('role.update', $role)}}">
            @method('PUT')
            @csrf
            @livewire('form::input-text', ['name' => 'name', 'labelText' => __('permission::role.role_name'), 'required'=>true,  'value' => $role->name])

            @livewire('form::dual-list-box', ['name' => 'users[]', 'idField' => 'roleUsersSelect', 'labelText' => __('permission::role.assign_users_to_role'), 'allValues' => $users, 'selectedValues' => $roleUsers, 'height' => '232px'])

        @foreach($modules as $module)
                <div class="table-responsive table-bpanel">
                    <table class="table table-hover table-2-columns">
                        <thead class="bgc-primary-d1 text-white ">
                        <tr>
                            <th scope="col" colspan="2">
                                <h4 class="text-120 mb-0">
                                    Permisos del módulo: <b>{{$module->title}}</b>
                                </h4>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(\Bittacora\Permission\PermissionFacade::getModulePermissions($module->key) as $permission)
                            <tr>
                                @if($module->key == 'role')
                                    <td>{{ __("permission::$permission->name") }}</td>
                                @else
                                    <td>{{ __("$module->key::$permission->name") }}</td>
                                @endif
                                <td class="text-center">
                                    @if(in_array($permission->id, $rolePermissions))
                                        @livewire('form::input-checkbox', ['name' => 'permission', 'multiple' => true,
                                        'idField' => $permission->name, 'value' => $permission->id, 'checked' => true])
                                    @else
                                        @livewire('form::input-checkbox', ['name' => 'permission', 'multiple' => true,
                                        'idField' => $permission->name, 'value' => $permission->id])
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endforeach


            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form::save-button',['theme'=>'update'])
                @livewire('form::save-button',['theme'=>'reset'])
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/jquery.bootstrap-duallistbox.js') }}"></script>
    <script>
        $(document).ready(function(){
            $("#select1").bootstrapDualListbox();
            $('.moveall i').removeClass().addClass('fa fa-arrow-right');
            $('.move i').removeClass().addClass('fa fa-arrow-right');
            $('.removeall i').removeClass().addClass('fa fa-arrow-left');
            $('.remove i').removeClass().addClass('fa fa-arrow-left');
        });
    </script>
@endsection
