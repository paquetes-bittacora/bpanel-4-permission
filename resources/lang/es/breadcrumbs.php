<?php

declare(strict_types=1);

return [
    'role' => 'Roles',
    'index' => 'Listado',
    'edit' => 'Editar rol',
    'create' => 'Crear rol',
    'show' => 'Ver rol',
];
