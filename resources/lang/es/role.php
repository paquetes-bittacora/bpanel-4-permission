<?php

return[
    'index' => 'Listar roles',
    'create' => 'Crear roles',
    'show' => 'Mostrar roles',
    'edit' => 'Editar roles',
    'destroy' => 'Borrar roles',
    'add' => 'Crear nuevo rol',
    'role_name' => 'Nombre',
    'role_list' => 'Listado de roles',
    'editing_role' => 'Editando rol',
    'module_permission_label' => 'Permisos del módulo: ',
    'assign_users_to_role' => 'Asignar rol a usuarios: ',
    'associated_users' => 'Usuarios asociados'
];
