<?php

return [
    'index' => 'Listar permisos',
    'create' => 'Crear permisos',
    'show' => 'Mostrar permisos',
    'edit' => 'Editar permisos',
    'destroy' => 'Borrar permisos'
];
